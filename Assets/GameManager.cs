﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnEnable() {
        EventManager.StartListening("playerdie", PlayerDie);
    }

    private void OnDisable() {
        EventManager.StopListening("playerdie", PlayerDie);
    }

    public void PlayerDie() {
        Debug.Log("YOU DIED");
        SceneManager.LoadScene("YouDied");
    }
}
