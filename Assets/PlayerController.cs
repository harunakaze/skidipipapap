﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float jumpPower;
    private Rigidbody2D rb;
    // Start is called before the first frame update

    bool isJump = false;
    bool isDown = false;
    bool isStop = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void OnEnable() {
        EventManager.StartListening("actiontwo", Jump);
    }

    private void OnDisable() {
        EventManager.StopListening("actiontwo", Jump);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.J)) {
            Jump();
        }

        if (Input.GetKeyDown(KeyCode.K)) {
            Stop();
        }

        if (Input.GetKeyDown(KeyCode.L)) {
            MoveDown();
        }


        if (isStop && Metronome.tickIsHappening) {
            isStop = false;
            MoveDown();
        }

        if (isJump && Metronome.tickIsHappening) {
            isJump = false;
            Stop();
            isStop = true;
        }
    }

    void Jump() {
        isJump = true;
        rb.AddForce(jumpPower * Vector2.up);
    }

    void MoveUp() {
        Jump();
    }

    void Stop() {
        rb.gravityScale = 0;
        rb.velocity = Vector2.zero;
    }

    void MoveDown() {
        rb.gravityScale = 10;
    }
}
