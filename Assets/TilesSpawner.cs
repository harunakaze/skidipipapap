﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilesSpawner : MonoBehaviour
{

    public GameObject tile;
    public GameObject tileTrap;
    public GameObject tileHolder;
    public Transform spawnLoc;

    private bool spawnNormal = true;
    private float saveTrapCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Metronome.tickIsHappening) {
            //Spawn();
        }
    }

    private void OnEnable() {
        EventManager.StartListening("actionone", Spawn);
        EventManager.StartListening("actiontwo", JumpSpawn);
        EventManager.StartListening("actionthree", Spawn);
    }

    private void OnDisable() {
        EventManager.StopListening("actionone", Spawn);
        EventManager.StartListening("actiontwo", JumpSpawn);
        EventManager.StartListening("actionthree", Spawn);
    }

    public void JumpSpawn() {
        //SpawnNormal();
        Spawn();
    }

    void SpawnNormal() {
        Debug.Log("SPAWN");
        spawnLoc.position = new Vector3(spawnLoc.position.x + 2.8f, spawnLoc.position.y, spawnLoc.position.z);
        GameObject newChild = null;


        newChild = Instantiate(tile, spawnLoc.position, Quaternion.identity);

        newChild.transform.parent = tileHolder.transform;

        newChild.GetComponent<TileChecker>().InitTile(TileChecker.TileType.NORMAL);
    }

    public void Spawn() {
        Debug.Log("SPAWN");
        spawnLoc.position = new Vector3(spawnLoc.position.x + 2.8f, spawnLoc.position.y, spawnLoc.position.z);
        GameObject newChild = null;

        float rand = Random.value;
        //Debug.Log(rand);

        if (rand < 0.8 || saveTrapCount != 0) {
            //if (spawnNormal) {
            if(saveTrapCount != 0)
                saveTrapCount--;

            newChild = Instantiate(tile, spawnLoc.position, Quaternion.identity);
            spawnNormal = false;
        } else {
            saveTrapCount = 4;
            newChild = Instantiate(tileTrap, spawnLoc.position, Quaternion.identity);
            spawnNormal = true;
        }

        newChild.transform.parent = tileHolder.transform;

        newChild.GetComponent<TileChecker>().InitTile(TileChecker.TileType.NORMAL);
    }
}
