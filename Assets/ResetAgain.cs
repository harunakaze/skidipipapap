﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ResetAgain : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("ResetLevel");
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator ResetLevel() {
        yield return new WaitForSeconds(2.0f);

        SceneManager.LoadScene("ScaleOne");
    }
}
