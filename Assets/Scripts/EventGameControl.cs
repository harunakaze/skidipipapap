﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventGameControl : MonoBehaviour
{
    public GameObject startGameScreen;
    public GameObject endGameScreen;
    public GameObject onPlayGameScreen;

    public bool isStartGame;
    public bool isEndGame;
    public bool isOnPlayGame;

    void Awake()
    {
        EventManager.StartListening("EnableStartGame", EnableStartGame);
        EventManager.StartListening("EnableOnPlayGame", EnableOnPlayGame); 
        EventManager.StartListening("EnableEndGame", EnableEndGame);
        EventManager.StartListening("DisableStartGame", DisableStartGame);
        EventManager.StartListening("DisableOnPlayGame", DisableOnPlayGame);
        EventManager.StartListening("DisableEndGame", DisableEndGame);
    }

    void EnableStartGame()
    {
        startGameScreen.SetActive(true);
        isStartGame = true;
    }

    void EnableEndGame()
    {
        endGameScreen.SetActive(true);
        isEndGame = true;
    }

    void EnableOnPlayGame()
    {
        onPlayGameScreen.SetActive(true);
        isOnPlayGame = true;
    }

    void DisableStartGame()
    {
        startGameScreen.SetActive(false);
        isStartGame = false;
    }

    void DisableEndGame()
    {
        endGameScreen.SetActive(false);
        isEndGame = false;
    }

    void DisableOnPlayGame()
    {
        onPlayGameScreen.SetActive(false);
        isOnPlayGame = false;
    }
}
