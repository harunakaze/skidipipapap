﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Rigidbody2D rigidbodyPlayer;
    private Collider2D colliderPlayer;
    private Animator animatorPlayer;

    public const string ANIMATION_JUMP = "animation_jump";
    public const string ANIMATION_HIT = "animation_hit";

    public KeyCode keyJump = KeyCode.Space;
    public KeyCode keyHit = KeyCode.LeftShift;

    void Start()
    {
        rigidbodyPlayer = GetComponent<Rigidbody2D>();
        colliderPlayer = GetComponent<Collider2D>();
        animatorPlayer = GetComponent<Animator>();
    }

    void Update()
    {
        //Debug.Log("My Score : " + EventPlayer.score);
        //EventManager.TriggerEvent("IncreaseScore");

        if (Input.GetKeyDown(keyJump))
        {
            EventManager.TriggerEvent("Jump");
            animatorPlayer.SetTrigger(ANIMATION_JUMP);
        }
        else if (Input.GetKeyDown(keyHit))
        {
            EventManager.TriggerEvent("Hit");
            animatorPlayer.SetTrigger(ANIMATION_HIT);
        }
    }
}
