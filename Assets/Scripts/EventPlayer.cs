﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventPlayer : MonoBehaviour {
    public static int score = 0;

    void Awake()
    {
        EventManager.StartListening("Jump", Jump);
        EventManager.StartListening("Hit", Hit);
        EventManager.StartListening("IncreaseScore", IncreaseScore);
    }

    void Jump()
    {
        Debug.Log("Jump!!!");
    }
    
    void Hit()
    {
        Debug.Log("Hit!!!");
    }

    void IncreaseScore()
    {
        Debug.Log("My Score : " + score);
        score++;
    }
}
