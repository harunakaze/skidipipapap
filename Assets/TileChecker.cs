﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileChecker : MonoBehaviour
{
    public enum TileType {
        NORMAL,
        TRAP
    }

    public TileType tileType;

    private bool tileDeactivated = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if(collision.gameObject.CompareTag("Player")) {
            DisarmTile();

            if(tileDeactivated) {
                Debug.Log("player is save");
            } else {
                TriggerTileEvent();
            }
        }
    }

    public void DisarmTile() {
        if (tileType == TileType.TRAP && Metronome.triggeredCommand == Metronome.TriggeredCommand.TWO) {
            tileDeactivated = true;

        }
        else if (tileType == TileType.NORMAL && Metronome.triggeredCommand == Metronome.TriggeredCommand.ONE) {
            tileDeactivated = true;
        }

        Metronome.triggeredCommand = Metronome.TriggeredCommand.NONE;
    }

    public void InitTile(TileType initType) {
        tileType = initType;

        if(tileType == TileType.TRAP) {
            Debug.Log("init as trap");
        } else if(tileType == TileType.NORMAL) {
            Debug.Log("init as normal");
        }
    }

    void TriggerTileEvent() {
        if (tileType == TileType.TRAP) {
            Debug.Log("trigger as trap");
        }
        else if (tileType == TileType.NORMAL) {
            Debug.Log("trigger as normal");
        }
    }


}
