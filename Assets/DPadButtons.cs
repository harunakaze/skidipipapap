﻿using UnityEngine;
using System.Collections;

public class DPadButtons : MonoBehaviour {
    public static bool up;
    public static bool down;
    public static bool left;
    public static bool right;

    float lastX;
    float lastY;
    //float lastDpadX;
    //float lastDpadY;

    void reset() {
        //up = down = left = right = false;
        lastX = Input.GetAxis("Horizontal");
        lastY = Input.GetAxis("Vertical");
    }

    void Update() {
        if (Input.GetAxis("Horizontal") == 1 && lastX != 1) { right = true; } else { right = false; }
        if (Input.GetAxis("Horizontal") == -1 && lastX != -1) { left = true; } else { left = false; }
        if (Input.GetAxis("Vertical") == 1 && lastY != 1) { up = true; } else { up = false; }
        if (Input.GetAxis("Vertical") == -1 && lastY != -1) { down = true; } else { down = false; }

        reset();
    }
}