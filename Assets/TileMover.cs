﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMover : MonoBehaviour
{
    // Transforms to act as start and end markers for the journey.
    //public Transform startMarker;
    //public Transform endMarker;

    public float endPosOffset = -2.8f;

    public TilesSpawner tileSpawner;

    private Vector3 startPos;
    private Vector3 endPos;

    // Movement speed in units/sec.
    public float speed = 1.0F;

    // Time when the movement started.
    private float startTime;

    // Total distance between the markers.
    private float journeyLength;

    private bool isMoving = false;

    // Start is called before the first frame update
    void Start()
    {
        Onward();
    }

    // Update is called once per frame
    void Update()
    {
        ProcessOnward();
        CheckOnward();
    }

    void CheckOnward() {
        if(Metronome.tickIsHappening && !isMoving) {
            Onward();
            //tileSpawner.Spawn();
        }
    }

    void ProcessOnward() {

        if(!isMoving) {
            return;
        }

        // Distance moved = time * speed.
        float distCovered = (Time.time - startTime) * speed;

        // Fraction of journey completed = current distance divided by total distance.
        float fracJourney = distCovered / journeyLength;

        // Set our position as a fraction of the distance between the markers.
        //Debug.Log(fracJourney);
        transform.position = Vector3.Lerp(startPos, endPos, fracJourney);

        if(transform.position == endPos) {
            isMoving = false;
        }
    }

    void Onward() {
        //this.transform.posit
        // Keep a note of the time the movement started.
        startTime = Time.time;

        startPos = transform.position;
        endPos = startPos;
        endPos.x += endPosOffset;

        // Calculate the journey length.
        journeyLength = Vector3.Distance(startPos, endPos);

        isMoving = true;
    }
}
