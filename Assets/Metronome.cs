﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Metronome : MonoBehaviour {
    public double bpm = 140.0F;
    public float gain = 0.5F;
    public int signatureHi = 4;
    public int signatureLo = 4;
    private double nextTick = 0.0F;
    private float amp = 0.0F;
    private float phase = 0.0F;
    private double sampleRate = 0.0F;
    private int accent;
    private bool running = false;

    public static TriggeredCommand triggeredCommand;

    public enum TriggeredCommand {
        NONE,
        ONE,
        TWO,
        THREE
    }

    public enum CommandInput {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        R1
    }

    public AudioSource tickSound;
    public AudioSource BGM;
    public float BPM_OLD = 100;
    public float tickOkZone = 0.2f;
    public float beatOffset = 0.0f;

    public List<CommandInput> actionOne = new List<CommandInput>();
    public List<CommandInput> actionTwo = new List<CommandInput>();
    public List<CommandInput> actionThree = new List<CommandInput>();


    private List<CommandInput> currentCommand = new List<CommandInput>();

    float beatTime;
    float lastTime = 0.0f;
    float timer = 0.0f;

    float inputTime = -1.0f;
    float tickTime;

    public static bool tickIsHappening = false;

    void Start() {
        BGM.Play();

        accent = signatureHi;
        double startTick = AudioSettings.dspTime;
        sampleRate = AudioSettings.outputSampleRate;
        nextTick = startTick * sampleRate;
        running = true;
    }

    void Update() {
        //BeatTimerCheck();
        Tick();
        commandInput();
        AcceptChecker();
    }

    void commandInput() {

        if (DPadButtons.right) {
            AddCommand(CommandInput.RIGHT);
        }

        if (DPadButtons.left) {
            AddCommand(CommandInput.LEFT);
        }

        if (DPadButtons.up) {
            AddCommand(CommandInput.UP);
        }

        if (DPadButtons.down) {
            AddCommand(CommandInput.DOWN);
        }
    }

    void AddCommand(CommandInput input) {
        currentCommand.Add(input);

        //Debug.Log("@@@");

        //foreach (var command in currentCommand) {
        //    Debug.Log(command);
        //}

        //Debug.Log("###");
    }

    void ExecuteCommand() {
        Debug.Log("HIT!");

        var actionOneTest = currentCommand.SequenceEqual(actionOne);
        var actionTwoTest = currentCommand.SequenceEqual(actionTwo);
        var actionThreeTest = currentCommand.SequenceEqual(actionThree);

        if (actionOneTest) {
            Debug.Log("Action One triggered!");
            triggeredCommand = TriggeredCommand.ONE;
            EventManager.TriggerEvent("actionone");
        }
        else if (actionTwoTest) {
            Debug.Log("Action Two triggered!");
            triggeredCommand = TriggeredCommand.TWO;
            EventManager.TriggerEvent("actiontwo");
        }
        else if (actionThreeTest) {
            Debug.Log("Action Three triggered!");
            triggeredCommand = TriggeredCommand.THREE;
            EventManager.TriggerEvent("actionthree");
        }
        else {
            Debug.Log("Wrong action sequence!");
            triggeredCommand = TriggeredCommand.NONE;

        }

        currentCommand.Clear();
    }

    void WrongExecuteTime() {
        Debug.Log("MISS!");
        currentCommand.Clear();
    }

    void AcceptChecker() {
        bool isHit = false;
        if (Input.GetButtonDown("ActionR1")) {
            inputTime = Time.timeSinceLevelLoad;
            isHit = true;
            //Debug.Log(Mathf.Abs(tickTime - inputTime));
        }

        float delta = Mathf.Abs(tickTime - inputTime);

        if (delta <= tickOkZone && isHit) {
            ExecuteCommand();
        }
        else if (isHit) {
            WrongExecuteTime();
        }

        inputTime = -1;
    }

    void OnAudioFilterRead(float[] data, int channels) {
        if (!running)
            return;

        double samplesPerTick = sampleRate * 60.0F / bpm * 4.0F / signatureLo;
        double sample = AudioSettings.dspTime * sampleRate;
        int dataLen = data.Length / channels;
        int n = 0;
        while (n < dataLen) {
            float x = gain * amp * Mathf.Sin(phase);
            int i = 0;
            while (i < channels) {
                data[n * channels + i] += x;
                i++;
            }
            while (sample + n >= nextTick) {
                nextTick += samplesPerTick;
                amp = 1.0F;
                if (++accent > signatureHi) {
                    accent = 1;
                    amp *= 2.0F;
                }
                //tickTime = Time.timeSinceLevelLoad;
                tickIsHappening = true;

                //Debug.Log("Tick: " + accent + "/" + signatureHi);
            }
            phase += amp * 0.3F;
            amp *= 0.993F;
            n++;
        }
    }

    void Tick() {
        if(tickIsHappening) {
            tickTime = Time.timeSinceLevelLoad;
            tickIsHappening = false;
        }
    }
}