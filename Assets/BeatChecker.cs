﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using UnityEngine;

public class BeatChecker : MonoBehaviour {

    public enum CommandInput {
        UP,
        DOWN,
        LEFT,
        RIGHT,
        R1
    }

    public AudioSource tickSound;
    public AudioSource BGM;
    public double BPM_OLD = 100;
    public double tickOkZone = 0.2f;
    public double beatOffset = 0.0f;

    public List<CommandInput> actionOne = new List<CommandInput>();
    public List<CommandInput> actionTwo = new List<CommandInput>();
    public List<CommandInput> actionThree = new List<CommandInput>();


    private List<CommandInput> currentCommand = new List<CommandInput>();

    double beatTime;
    double lastTime = 0.0f;
    double timer = 0.0f;

    double inputTime = -1.0f;
    double tickTime;

    float averageHitDelta = 0.0f;

    public double bpm = 140.0F;
    public float gain = 0.5F;
    public int signatureHi = 4;
    public int signatureLo = 4;
    private double nextTick = 0.0F;
    private float amp = 0.0F;
    private float phase = 0.0F;
    private double sampleRate = 0.0F;
    private int accent;
    private bool running = false;

    //bool rightPressed = false;
    //bool leftPressed = false;
    //bool upPressed = false;
    //bool downPressed = false;

    private void Start() {
        //BGM.timeSamples = 165286;
        BGM.Play();
        beatTime = 60 / BPM_OLD;
        Debug.Log(beatTime);

        accent = signatureHi;
        double startTick = AudioSettings.dspTime;
        sampleRate = AudioSettings.outputSampleRate;
        nextTick = startTick * sampleRate;
        running = true;
    }

    void Update () {
        //BeatTimerCheck();
        commandInput();
        AcceptChecker();
    }

    void commandInput() {

        if(DPadButtons.right) {
            AddCommand(CommandInput.RIGHT);
        }

        if (DPadButtons.left) {
            AddCommand(CommandInput.LEFT);
        }

        if (DPadButtons.up) {
            AddCommand(CommandInput.UP);
        }

        if (DPadButtons.down) {
            AddCommand(CommandInput.DOWN);
        }
    }

    void AddCommand(CommandInput input) {
        currentCommand.Add(input);

        //Debug.Log("@@@");

        //foreach (var command in currentCommand) {
        //    Debug.Log(command);
        //}

        //Debug.Log("###");
    }

    void ExecuteCommand() {
        Debug.Log("HIT!");

        var actionOneTest = currentCommand.SequenceEqual(actionOne);
        var actionTwoTest = currentCommand.SequenceEqual(actionTwo);
        var actionThreeTest = currentCommand.SequenceEqual(actionThree);

        if (actionOneTest) {
            Debug.Log("Action One triggered!");
        } else if(actionTwoTest) {
            Debug.Log("Action Two triggered!");
        } else if(actionThreeTest) {
            Debug.Log("Action Three triggered!");
        } else {
            Debug.Log("Wrong action sequence!");
        }

        currentCommand.Clear();
    }

    void WrongExecuteTime() {
        Debug.Log("MISS!");
        currentCommand.Clear();
    }

    void AcceptChecker() {
        bool isHit = false;
        if (Input.GetButtonDown("ActionR1")) {
            inputTime = AudioSettings.dspTime;
            isHit = true;
            //Debug.Log(Mathf.Abs(tickTime - inputTime));
        }

        double delta = Math.Abs(tickTime - inputTime);

        if (delta <= tickOkZone && isHit) {
            ExecuteCommand();
        } else if(isHit) {
            WrongExecuteTime();
        }

        inputTime = -1;
    }

    void BeatTimerCheck() {
        //timer += Time.deltaTime;
        timer += BGM.time - lastTime;

        if(timer >= beatTime + beatOffset) {
            //Debug.Log("Beat");
            tickSound.Play();

            tickTime = Time.timeSinceLevelLoad;

            timer -= beatTime + beatOffset;
        }

        lastTime = BGM.time;
    }

    void OnAudioFilterRead(float[] data, int channels) {
        return;
        if (!running)
            return;

        double samplesPerTick = sampleRate * 60.0F / bpm * 4.0F / signatureLo;
        double sample = AudioSettings.dspTime * sampleRate;
        int dataLen = data.Length / channels;
        int n = 0;
        while (n < dataLen) {
            float x = gain * amp * Mathf.Sin(phase);
            int i = 0;
            while (i < channels) {
                data[n * channels + i] += x;
                i++;
            }
            while (sample + n >= nextTick) {
                nextTick += samplesPerTick;
                amp = 1.0F;
                if (++accent > signatureHi) {
                    accent = 1;
                    amp *= 2.0F;
                }
                tickTime = Time.timeSinceLevelLoad;
                Debug.Log("Tick: " + accent + "/" + signatureHi);
            }
            phase += amp * 0.3F;
            amp *= 0.993F;
            n++;
        }
    }
}
